[gearbox.commands]
setup-app = gearbox.commands.setup_app:SetupAppCommand
serve = gearbox.commands.serve:ServeCommand
makepackage = gearbox.commands.basic_package:MakePackageCommand

[console_scripts]
gearbox = gearbox.main:main

[paste.server_runner]
cherrypy = gearbox.commands.serve:cherrypy_server_runner
wsgiref = gearbox.commands.serve:wsgiref_server_runner

[paste.server_factory]
gevent = gearbox.commands.serve:gevent_server_factory

