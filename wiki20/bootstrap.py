# -*- coding: utf-8 -*-
"""Setup the wiki20 application"""
from __future__ import print_function

from wiki20 import model
from tgext.pluggable import app_model

import transaction

def bootstrap(command, conf, vars):
    print('Bootstrapping wiki20...')
    from sqlalchemy.exc import IntegrityError
    try:
        user = app_model.User()
        user.user_name = 'viniciusm'
        user.email_address = 'viniciusm@escola.mpu.mp.br'
        user.display_name = u'Vinícius Moreira'
        user.password = '123456'
        model.DBSession.add(user)

        s1 = model.Sample()
        s1.name = 'Test Sample'
        s1.user = user

        p1 = model.Page()
        p1.pagename = 'First Page'
        p1.pagecontent = u'Ahhh Muleeeequeeee!\nEssa bagaça está funcionando!!!'
        p1.user = user

        model.DBSession.add(s1)
        model.DBSession.add(p1)
        model.DBSession.flush()
    except IntegrityError:
        print('Warning, there was a problem adding your new sample data, it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')