# -*- coding: utf-8 -*-
"""Main Controller"""
import re
from docutils.core import publish_parts

from tg import TGController
from tg import expose, flash, require, url, lurl, request, redirect, validate
from tg.i18n import ugettext as _, lazy_ugettext as l_

from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from tgext.pluggable import app_model

from wiki20 import model
from wiki20.model import DBSession

wikiwords = re.compile(r"\b([A-Z]\w+[A-Z]+\w+)")

class RootController(TGController):
    @expose('wiki20.templates.page')
    def _default(self, pagename = 'First Page'):
        from sqlalchemy.exc import InvalidRequestError
        try:
            page = DBSession.query(model.Page).filter_by(pagename = pagename).one()
        except InvalidRequestError:
            raise redirect('notfound', pagename = pagename)

        pagecontent = publish_parts(page.pagecontent, writer_name = "html")["html_body"]
        root = url('/wiki20/')
        pagecontent = wikiwords.sub(r'<a href="%s\1">\1</a>'%root , pagecontent)
        return dict(pagecontent=pagecontent, page=page)


    @expose('wiki20.templates.edit_page')
    def edit_page(self, pagename):
        page = DBSession.query(model.Page).filter_by(pagename = pagename).one()
        return dict(page=page)

    @expose()
    def save_page(self, pagename, pagecontent, submit):
        page = DBSession.query(model.Page).filter_by(pagename = pagename).one()
        page.pagecontent = pagecontent

        redirect('/wiki20?pagename=' + pagename)

    @expose('wiki20.templates.edit_page')
    def notfound(self , pagename):
        try:
            identity = request.environ.get('repoze.who.identity')
            userid = identity['user'].user_id
        except:
            userid = None

        page = model.Page()
        page.pagename = pagename
        page.pagecontent = ''
        page.user_id = userid

        DBSession.add(page)

        return dict(page = page)

    @expose('wiki20.templates.list_pages')
    def list_pages(self):
        pages = [page.pagename for page in DBSession.query(model.Page).order_by(model.Page.pagename)]
        return dict(pages = pages)