from tg import expose

@expose('wiki20.templates.little_partial')
def something(name):
    return dict(name=name)